package main

import (
	"flag"
	"fmt"
	"gitlab.com/olivierva/narrowboat-server/internal/relay"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
)

func main() {

	var address = flag.String("address", "localhost:9090", "Address the server should listen on [ip:port]")
	var rProxyAddress = flag.String("rproxy", "localhost:8080", "Address of web client server")

	flag.Parse()

	log.SetFlags(log.Lshortfile)

	// websocket server
	server := relay.NewServer("/entry")
	go server.Listen()

	rpUrl, err := url.Parse(fmt.Sprintf("http://%s", *rProxyAddress))
	if err != nil {
		log.Fatal(err)
	}

	http.Handle("/", httputil.NewSingleHostReverseProxy(rpUrl))

	// static files
	log.Printf("Starting  server on http://%s", *address)
	log.Printf("Websocket listening on ws://%s/entry", *address)
	log.Printf("Reverse proxy web client from http://%s", *rProxyAddress)
	log.Fatal(http.ListenAndServe(*address, nil))

}
