#!/usr/bin/env bash
if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 1
fi
sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token $1 \
  --executor docker \
  --description "My Docker Runner" \
  --docker-image "docker:latest" \
  --docker-privileged

