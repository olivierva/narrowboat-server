define(
	"Hexagon",
	[],
	function() {

		function Hexagon(model) {
			if (model !== undefined) {
				this.hextype = ko.observable(model.hextype).extend({ numeric: 0 });
				this.q = ko.observable(model.q).extend({ numeric: 0 });
				this.r = ko.observable(model.r).extend({ numeric: 0 });
				this.s = ko.observable(model.s).extend({ numeric: 0 });
				this.errorMsg = ko.observable(model.errorMsg).extend({text: ""})
			} else {
                this.hextype = ko.observable(this.hextype).extend({ numeric: 0 });
                this.q = ko.observable(this.q).extend({ numeric: 0 });
                this.r = ko.observable(this.r).extend({ numeric: 0 });
                this.s = ko.observable(this.s).extend({ numeric: 0 });
                this.errorMsg = ko.observable("").extend({ text: "" });
            }

            this.toModel = function() {
				return {
					hextype: this.hextype(),
					q: this.q(),
					r: this.r(),
					s: this.s(),
					errorMsg: this.errorMsg()
				};
			}
		}

		return Hexagon;
	}
);
