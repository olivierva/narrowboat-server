define(
	"HexagonList",
	[
		"Hexagon"
	],
	function(Hexagon) {

		function HexagonList(ws) {
			var that = this;
			this.hexes = ko.observableArray();

            this.addHexagon = ko.observable(new Hexagon());

            this.send = function() {
                var model = this.addHexagon().toModel();
                console.warn(model.q + model.r + model.s);
                if(Number(model.q) + Number(model.r) + Number(model.s) != 0) {
                    console.warn("Error cube not zero");
                    model.errorMsg = "Error cube not zero";
                } else {
                    console.warn(JSON.stringify(model, replacer));
                    ws.send(JSON.stringify(model, replacer));
                    var hex = new Hexagon(model);
                    this.addHexagon(hex);
                }

			};

			ws.onmessage = function(e) {
				var model = JSON.parse(e.data);
				var hex = new Hexagon(model);
				console.warn(hex);
				that.hexes.push(hex);
			};
		}
		
		return HexagonList;
	}
);

function replacer(key, value) {
    if (typeof value === 'string') {
        return new Number(value);
    }
    return value;
}
