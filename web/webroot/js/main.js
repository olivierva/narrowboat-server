define(
    "main",
    [
        "HexagonList"
    ],
    function(HexagonList) {
        var ws = new WebSocket("ws://localhost:9090/entry");
        var list = new HexagonList(ws);
        ko.applyBindings(list);
    }
);
