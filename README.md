# Server side software for the Narrowboat Simulator

## Building
```docker build -f ./build/package/Dockerfile -t registry.gitlab.com/olivierva/narrowboat-server .```

## Running
```docker run -p 8080:8080 registry.gitlab.com/olivierva/narrowboat-server ```

## Frontend
<http://localhost:8080>

