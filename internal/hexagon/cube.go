package hexagon

import "log"
import "github.com/satori/go.uuid"

type Cube struct {
	HexType HexType   `json:"hextype"`
	Action  Action    `json:"action"`
	X       int64     `json:"x"`
	Y       int64     `json:"y"`
	Z       int64     `json:"z"`
	Px      int64     `json:px`
	Py      int64     `json:py`
	Radius  int64     `json:"radius"`
	Uuid    uuid.UUID `json:"uuid"`
}

func NewCube(ht HexType, action Action, x int64, y int64, z int64, radius int64) *Cube {
	return &Cube{ht, action, x, y, z, 0, 0, radius, uuid.NewV4()}
}

func (c *Cube) EqualLocation(lc *Cube) bool {
	if c.X == lc.X && c.Y == lc.Y && c.Z == lc.Z {
		return true
	}
	return false
}

func (c *Cube) Add(a *Cube) *Cube {
	return &Cube{HexType: Plain,
		X: c.X + a.X,
		Y: c.Y + a.Y,
		Z: c.Z + a.Z,
	}
}

func (c *Cube) Subtract(a *Cube) *Cube {
	return &Cube{HexType: Plain,
		X: c.X - a.X,
		Y: c.Y - a.Y,
		Z: c.Z - a.Z,
	}
}

func (c *Cube) Multiply(a *Cube) *Cube {
	return &Cube{HexType: Plain,
		X: c.X * a.X,
		Y: c.Y * a.Y,
		Z: c.Z * a.Z,
	}
}

func (c *Cube) Length() int64 {
	return ((absInt64(c.X) + absInt64(c.Y) + absInt64(c.Z)) / 2)
}

func (c *Cube) Distance(a *Cube) int64 {
	return c.Subtract(a).Length()
}

func (c *Cube) Scale(radius int) *Cube {
	return &Cube{HexType: Plain,
		X: c.X * int64(radius),
		Y: c.Y * int64(radius),
		Z: c.Z * int64(radius),
	}
}

func (c *Cube) Ring(radius int) (results []Cube) {
	cDirection := c.Direction(4)
	cScale := cDirection.Scale(int(c.Radius))
	cube := c.Add(cScale)

	for i := 0; i < 6; i++ {
		for j := 0; j < int(c.Radius); j++ {
			log.Println("Ring:", cube)
			results = append(results, *cube)
			cube = cube.Neighbor(i)
		}
	}

	return results
}

var cubeDirections = []Cube{
	Cube{0, Add, 1, -1, 0, 0, 0, 0, uuid.Nil},
	Cube{0, Add, 1, 0, -1, 0, 0, 0, uuid.Nil},
	Cube{0, Add, 0, 1, -1, 0, 0, 0, uuid.Nil},
	Cube{0, Add, -1, 1, 0, 0, 0, 0, uuid.Nil},
	Cube{0, Add, -1, 0, 1, 0, 0, 0, uuid.Nil},
	Cube{0, Add, 0, -1, +1, 0, 0, 0, uuid.Nil},
}

func (c *Cube) Direction(direction int) *Cube {
	return &cubeDirections[direction]
}

func (c *Cube) Neighbor(direction int) *Cube {
	return c.Add(c.Direction(direction))
}

func absInt64(n int64) int64 {
	y := n >> 63
	return (n ^ y) - y
}
