package hexagon

import (
	"github.com/satori/go.uuid"
	"math"
	"testing"
)

func TestNewCube(t *testing.T) {
	result1 := NewCube(Plain, 1, 1, 1, 1, Add)
	result2 := NewCube(Plain, 1, 1, 1, 1, Add)

	if result1.Uuid != uuid.Nil {
		t.Errorf("Uuid value is not generated")
	}

	if result2.Uuid != uuid.Nil {
		t.Errorf("Uuid value is not generated")
	}

	if result1.Uuid == result1.Uuid {
		t.Errorf("Uuids should be unique, got %s for both cubes", result1.Uuid)
	}

}

func TestAbsInt64Negative(t *testing.T) {
	input := int64(-10)
	expected := int64(10)

	result := absInt64(input)
	if result != expected {
		t.Errorf("Absolute was incorrect, got %d, want %d", result, expected)
	}
}

func TestAbsInt64Positive(t *testing.T) {
	input := int64(10)
	expected := int64(10)

	result := absInt64(input)
	if result != expected {
		t.Errorf("Absolute was incorrect, got %d, want %d", result, expected)
	}
}

func TestAbsInt64NegativeMin(t *testing.T) {
	input := int64(math.MinInt64 + 1)
	expected := int64(math.MaxInt64)

	result := absInt64(input)
	if result != expected {
		t.Errorf("Absolute was incorrect, got %d, want %d", result, expected)
	}
}

func TestAbsInt64PositiveMax(t *testing.T) {
	input := int64(math.MaxInt64)
	expected := int64(math.MaxInt64)

	result := absInt64(input)
	if result != expected {
		t.Errorf("Absolute was incorrect, got %d, want %d", result, expected)
	}
}

func TestCube_EqualLocation(t *testing.T) {
	cube := Cube{Plain, 1, 2, 3, 0, uuid.Nil, Add}
	c1 := Cube{Plain, 1, 2, 3, 0, uuid.Nil, Add}
	if !cube.EqualLocation(&c1) {
		t.Errorf("Cubes should be equal, got %#v != %#v", cube, c1)
	}

	c2 := Cube{Plain, 3, 2, 1, 0, uuid.Nil, Add}
	if cube.EqualLocation(&c2) {
		t.Errorf("Cubes should not be equal, got %#v == %#v", cube, c2)
	}

	c3 := Cube{CanalLock, 1, 2, 3, 0, uuid.Nil, Add}
	if !cube.EqualLocation(&c3) {
		t.Errorf("Cubes should be equal despite different type, got %#v != %#v", cube, c3)
	}
}

func TestCube_Add(t *testing.T) {
	input1 := Cube{Plain, 1, -1, 0, 0, uuid.Nil, Add}
	input2 := Cube{Plain, 2, -1, -1, 0, uuid.Nil, Add}
	expected := Cube{Plain, 3, -2, -1, 0, uuid.Nil, Add}
	output := input1.Add(&input2)
	if !output.EqualLocation(&expected) {
		t.Errorf("Cubes should be equal despite different type, got %#v expected %#v", output, expected)
	}

}
