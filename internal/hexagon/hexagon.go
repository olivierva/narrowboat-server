package hexagon

type HexType int

const (
	Plain HexType = iota
	CanalStraight
	CanalStraightBridge
	CanalCurveLeft
	CanalCurveRight
	CanalCurveSplit
	CanalStraightCurveLeft
	CanalStraightCurveRight
	CanalLock
	CanalTunnel
	CanalSlope
	Ring
)

type Action int

const (
	Add Action = iota
	Del
	AddRing
	DelRing
)
