package relay

import (
	"fmt"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/olivierva/narrowboat-server/internal/hexagon"
	"golang.org/x/net/websocket"
	"io"
	"log"
	"math"
)

const channelBufSize = 100

var maxId int64 = 0

// Chat client.
type Client struct {
	id          int64
	ClientInfo  *ClientInfo
	ws          *websocket.Conn
	server      *Server
	ch          chan hexagon.Cube
	doneCh      chan bool
	initialized bool
}

type ClientInfo struct {
	HexRadius int       `json:"hexradius"`
	ClientId  uuid.UUID `json:uuid`
}

// Create new chat client.
func NewClient(ws *websocket.Conn, server *Server) *Client {

	if ws == nil {
		panic("ws cannot be nil")
	}

	if server == nil {
		panic("server cannot be nil")
	}

	maxId++
	ch := make(chan hexagon.Cube, channelBufSize)
	doneCh := make(chan bool)

	return &Client{maxId, &ClientInfo{}, ws, server, ch, doneCh, false}
}

func (c *Client) Conn() *websocket.Conn {
	return c.ws
}

func (c *Client) Write(msg *hexagon.Cube) {
	select {
	case c.ch <- *msg:
	default:
		c.server.Del(c)
		err := fmt.Errorf("client %d is disconnected.", c.id)
		c.server.Err(err)
	}
}

func (c *Client) Done() {
	c.doneCh <- true
}

// Listen Write and Read request via chanel
func (c *Client) Listen() {
	go c.listenWrite()
	c.listenRead()
}

// Listen write request via chanel
func (c *Client) listenWrite() {
	log.Println("Listening write to client")
	for {
		select {

		// send message to the client
		case hex := <-c.ch:
			log.Println("Hex:", hex)

			// convert cube to XY coordinates
			nHex := c.ConvertToXY(&hex)
			log.Println("Send:", nHex)

			websocket.JSON.Send(c.ws, nHex)

			// receive done request
		case <-c.doneCh:
			c.server.Del(c)
			c.doneCh <- true // for listenRead method
			return
		}
	}
}

/**
Julia code:
function cube2oddr(cube::Cube, radius::Float64)
    height = 2 * radius
    width = sqrt(3) * height

    # calculate offset coordinates
    col = cube.x +(cube.z + (cube.z&1)) / 2
    row = -1 * cube.z

    y = row * height * 3/4
    x = col * width - row&1 * (width/2)

    return round(x)/2, round(y)
end
*/
func (c *Client) ConvertToXY(cube *hexagon.Cube) *hexagon.Cube {
	height := 2 * float64(c.ClientInfo.HexRadius)
	width := math.Sqrt(3) * height

	col := cube.X + (cube.Z+(cube.Z&1))/2
	row := -1 * cube.Z

	y := float64(row) * height * 0.75
	x := (float64(col)*width - float64(row&1)*(width/2)) / 2

	cube.Px = int64(x)
	cube.Py = int64(y)

	return cube

}

// Listen read request via chanel
func (c *Client) listenRead() {
	log.Println("Listening read from client")
	for {
		select {

		// receive done request
		case <-c.doneCh:
			c.server.Del(c)
			c.doneCh <- true // for listenWrite method
			return

			// read data from websocket connection
		default:
			if c.initialized {
				var hex hexagon.Cube
				err := websocket.JSON.Receive(c.ws, &hex)
				log.Printf("hex type: %d", hex.HexType)
				if err == io.EOF {
					c.doneCh <- true
				} else if err != nil {
					c.server.Err(err)
				} else if hex.HexType == hexagon.Ring {
					ring := hex.Ring(1)
					for _, cube := range ring {
						//time.Sleep(1 * time.Second)
						c.server.sendAll(&cube)
					}
				} else {
					c.server.SendAll(&hex)
				}
			} else {
				var cinfo ClientInfo
				err := websocket.JSON.Receive(c.ws, &cinfo)
				if err == io.EOF {
					c.doneCh <- true
				} else if err != nil {
					c.server.Err(err)
				} else {
					c.initialized = true
					if cinfo.ClientId == uuid.Nil {
						cinfo.ClientId = uuid.NewV4()
					}
					c.ClientInfo = &cinfo
					log.Printf("Hex size equals %d", cinfo.HexRadius)
				}
			}
		}
	}
}
