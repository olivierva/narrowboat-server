package relay

import (
	"gitlab.com/olivierva/narrowboat-server/internal/hexagon"
	"testing"
)

//
// #    x	 y	 z		 X	      Y
// 0   +1	-1 	  0		 3464	 0
// 1   +1	 0	 -1		 1732	 1500
// 2    0	+1	 -1		-1732	 1500
// 3   -1	+1	  0		-3464	 0
// 4   -1	 0	 +1		-1732	-1500
// 5    0	-1	 +1		 1732	-1500
//
func Test_00_Client_ConvertToXY(t *testing.T) {
	cube := hexagon.NewCube(hexagon.Plain, 1, -1, 0, 0, hexagon.Add)
	expectedX := int64(3464)
	expectedY := int64(0)

	client := Client{1, nil, nil, nil, nil, 1000, true}
	resultCube := client.ConvertToXY(cube)

	if resultCube.X != expectedX || resultCube.Y != expectedY {
		t.Errorf("Expected X: %d, Y: %d got X: %d, Y: %d", expectedX, expectedY, resultCube.X, resultCube.Y)
	}

}

//
// #    x	 y	 z		 X	      Y
// 0   +1	-1 	  0		 3464	 0
// 1   +1	 0	 -1		 1732	 1500
// 2    0	+1	 -1		-1732	 1500
// 3   -1	+1	  0		-3464	 0
// 4   -1	 0	 +1		-1732	-1500
// 5    0	-1	 +1		 1732	-1500
//
func Test_01_Client_ConvertToXY(t *testing.T) {
	cube := hexagon.NewCube(hexagon.Plain, 1, 0, -1, 1, hexagon.Add)
	expectedX := int64(1732)
	expectedY := int64(1500)

	client := Client{1, nil, nil, nil, nil, 1000, true}
	resultCube := client.ConvertToXY(cube)

	if resultCube.X != expectedX || resultCube.Y != expectedY {
		t.Errorf("Expected X: %d, Y: %d got X: %d, Y: %d", expectedX, expectedY, resultCube.X, resultCube.Y)
	}

}

//
// #    x	 y	 z		 X	      Y
// 0   +1	-1 	  0		 3464	 0
// 1   +1	 0	 -1		 1732	 1500
// 2    0	+1	 -1		-1732	 1500
// 3   -1	+1	  0		-3464	 0
// 4   -1	 0	 +1		-1732	-1500
// 5    0	-1	 +1		 1732	-1500
//
func Test_02_Client_ConvertToXY(t *testing.T) {
	cube := hexagon.NewCube(hexagon.Plain, 0, 1, -1, 1, hexagon.Add)
	expectedX := int64(-1732)
	expectedY := int64(1500)

	client := Client{1, nil, nil, nil, nil, 1000, true}
	resultCube := client.ConvertToXY(cube)

	if resultCube.X != expectedX || resultCube.Y != expectedY {
		t.Errorf("Expected X: %d, Y: %d got X: %d, Y: %d", expectedX, expectedY, resultCube.X, resultCube.Y)
	}

}

//
// #    x	 y	 z		 X	      Y
// 0   +1	-1 	  0		 3464	 0
// 1   +1	 0	 -1		 1732	 1500
// 2    0	+1	 -1		-1732	 1500
// 3   -1	+1	  0		-3464	 0
// 4   -1	 0	 +1		-1732	-1500
// 5    0	-1	 +1		 1732	-1500
//
func Test_03_Client_ConvertToXY(t *testing.T) {
	cube := hexagon.NewCube(hexagon.Plain, -1, 1, 0, 1, hexagon.Add)
	expectedX := int64(-3464)
	expectedY := int64(0)

	client := Client{1, nil, nil, nil, nil, 1000, true}
	resultCube := client.ConvertToXY(cube)

	if resultCube.X != expectedX || resultCube.Y != expectedY {
		t.Errorf("Expected X: %d, Y: %d got X: %d, Y: %d", expectedX, expectedY, resultCube.X, resultCube.Y)
	}

}

//
// #    x	 y	 z		 X	      Y
// 0   +1	-1 	  0		 3464	 0
// 1   +1	 0	 -1		 1732	 1500
// 2    0	+1	 -1		-1732	 1500
// 3   -1	+1	  0		-3464	 0
// 4   -1	 0	 +1		-1732	-1500
// 5    0	-1	 +1		 1732	-1500
//
func Test_04_Client_ConvertToXY(t *testing.T) {
	cube := hexagon.NewCube(hexagon.Plain, -1, 0, 1, 1, hexagon.Add)
	expectedX := int64(-1732)
	expectedY := int64(-1500)

	client := Client{1, nil, nil, nil, nil, 1000, true}
	resultCube := client.ConvertToXY(cube)

	if resultCube.X != expectedX || resultCube.Y != expectedY {
		t.Errorf("Expected X: %d, Y: %d got X: %d, Y: %d", expectedX, expectedY, resultCube.X, resultCube.Y)
	}

}

//
// #    x	 y	 z		 X	      Y
// 0   +1	-1 	  0		 3464	 0
// 1   +1	 0	 -1		 1732	 1500
// 2    0	+1	 -1		-1732	 1500
// 3   -1	+1	  0		-3464	 0
// 4   -1	 0	 +1		-1732	-1500
// 5    0	-1	 +1		 1732	-1500
//
func Test_05_Client_ConvertToXY(t *testing.T) {
	cube := hexagon.NewCube(hexagon.Plain, 0, -1, 1, 1, hexagon.Add)
	expectedX := int64(1732)
	expectedY := int64(-1500)

	client := Client{1, nil, nil, nil, nil, 1000, true}
	resultCube := client.ConvertToXY(cube)

	if resultCube.X != expectedX || resultCube.Y != expectedY {
		t.Errorf("Expected X: %d, Y: %d got X: %d, Y: %d", expectedX, expectedY, resultCube.X, resultCube.Y)
	}

}
